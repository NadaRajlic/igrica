package app;

import java.io.Serializable;
import java.util.Scanner;

import apstrakcije.Korisnik;

public class Administrator extends Korisnik implements Serializable {
	public Administrator() {
		super();
		uDatoteku();
	}

	public Administrator(String ime, String prezime, String korisnickoIme, String lozinka) {
		super(ime, prezime, korisnickoIme, lozinka);
		uDatoteku();
	}

	private static final long serialVersionUID = -6665217941415792862L;

	public void dodajRestoran() {
		Scanner sken = new Scanner(System.in);
		String uNaziv = "";
		int uBesplatno = 0;
		System.out.println("Unesi naziv restorana: ");
		try {
			uNaziv = sken.nextLine();
		} catch (Exception e) {
			System.out.println("Greska pri unosu.");
			return;
		}
		System.out.println("Unesi minimalan iznos za besplatnu dostavu:");
		try {
			uBesplatno = sken.nextInt();
		} catch (Exception e) {
			System.out.println("Greska pri unosu.");
			return;
		}
		sken.nextLine();
		new Restoran(uNaziv, uBesplatno);
	}

	public void kreirajKorisnika() {
		Scanner sken = new Scanner(System.in);
		int tipK;
		System.out.println("Unesi tip korisnika koji se kreira: 1. Kupac 2. Vlasnik restorana 3. Administrator");
		try {
			tipK = sken.nextInt();
		} catch (Exception e) {
			System.out.println("Greska pri unosu.");
			// e.printStackTrace();
			return;
		}
		sken.nextLine();

		String ime = "", prezime = "", korisnicko = "", lozinka = "";
		try {
			System.out.println("Unesi ime:");
			ime = sken.nextLine();
			System.out.println("Unesi prezime: ");
			prezime = sken.nextLine();
			System.out.println("Unesi korisnicko ime:");
			korisnicko = sken.nextLine();
			System.out.println("Unesi lozinku:");
			lozinka = sken.nextLine();
		} catch (Exception e) {
			System.out.println("Morate pravilno uneti podatke.");
			return;
		}
		boolean jedinstven = true;
		for (int i = 0; i < korisnici.size(); i++) {
			if (korisnicko.compareTo(korisnici.get(i).getKorisnickoIme()) == 0)
				jedinstven = false;
		}
		if (!jedinstven) {
			System.out.println("Korisnicko ime mora biti jedinstveno.");
			return;
		}
		switch (tipK) {
		case 1:
			String adresa;
			System.out.println("Unesi adresu:");
			try {
				adresa = sken.nextLine();
			} catch (Exception e) {
				System.out.println("Greska pri unosu.");
				return;
			}
			Kupac k = new Kupac(ime, prezime, korisnicko, lozinka);
			k.setKucnaAdresa(adresa);
			k.setNovcanoStanje(10000);
			break;
		case 2:
			VlasnikRestorana v = new VlasnikRestorana(ime, prezime, korisnicko, lozinka);
			break;
		case 3:
			Administrator a = new Administrator(ime, prezime, korisnicko, lozinka);
			break;
		default:
			System.out.println("Pogresno unet tip korisnika.");
		}
	}

	public void dodajVlasnika() {
		Scanner sken = new Scanner(System.in);
		String vKorisnicko = "";
		String rIme = "";
		System.out.println("Unesi korisnicko ime vlasnika kome se dodeljuje restoran:");
		try {
			vKorisnicko = sken.nextLine();
		} catch (Exception e) {
			System.out.println("Greska pri unosu");
			return;
		}
		int i = 0, j = 0;
		boolean postojiV = false, postojiR = false;
		while (i < Korisnik.korisnici.size() && !postojiV) {
			if (vKorisnicko.compareTo(Korisnik.korisnici.get(i).getKorisnickoIme()) == 0) {
				postojiV = true;
			} else
				i++;
		}
		if (!postojiV) {
			System.out.println("Trazeni vlasnik ne postoji.");
			return;
		} else {
			System.out.println("Unesi naziv restorana koji zelis da dodelis " + vKorisnicko + ":");
			try {
				rIme = sken.nextLine();
			} catch (Exception e) {
				System.out.println("Greska pri unosu.");
				return;
			}
			while (j < Restoran.restorani.size() && !postojiR) {
				if (rIme.compareTo(Restoran.restorani.get(j).naziv) == 0) {
					postojiR = true;
				} else
					j++;
			}
			if (!postojiR) {
				System.out.println("Trazeni restoran ne postoji.");
				return;
			} else {
				try {
					VlasnikRestorana v = (VlasnikRestorana) (Korisnik.korisnici.get(i)).clone();
					Restoran r = (Restoran) (Restoran.restorani.get(j)).clone();
					r.setVlasnik(v);
					v.restoraniVlasnika.add(r);
					Korisnik.korisnici.set(i, v);
					Restoran.restorani.set(j, r);
					uDatotekuIzmene();
					Restoran.uDatotekuIzmene();
					System.out.println("Restoran " + rIme + " uspesno dodeljen vlasniku " + vKorisnicko);

				} catch (CloneNotSupportedException e) {
					e.printStackTrace();
				}

			}
		}
	}
}
