package app;

import java.io.Serializable;
import apstrakcije.Stavka;
import enumeracije.JedinicaMere;
import enumeracije.TipJela;

public class Jelo extends Stavka implements Serializable {

	public Jelo() {

	}

	public Jelo(String naziv, int kolicina, JedinicaMere jedinicaMere, int cena, TipJela tipJela) {
		super();
		this.naziv = naziv;
		this.kolicina = kolicina;
		this.jedinicaMere = jedinicaMere;
		this.cena = cena;
		this.tipJela = tipJela;
	}

	TipJela tipJela;
	private static final long serialVersionUID = -6665217941415792869L;

	public TipJela getTipJela() {
		return tipJela;
	}

	public void setTipJela(TipJela tipJela) {
		this.tipJela = tipJela;
	}

	@Override
	public void prikaziDetaljno() {
		System.out.println("Naziv: " + naziv + ", kolicina: " + kolicina + ", jedinica mere: " + jedinicaMere.toString()
				+ ", cena: " + cena + "Tip: " + tipJela);
	}

}
