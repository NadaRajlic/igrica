package app;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Scanner;

import apstrakcije.Prikaz;

public class Korpa implements Prikaz, Serializable {

	public Korpa() {
		super();
		this.kupac = new Kupac();
	}

	public Korpa(Kupac kupac) {
		super();
		this.kupac = kupac;
		this.zaRestoran = "";
	}

	public Korpa(Kupac kupac, ArrayList<StavkaUKorpi> stavkeUKorpi, String zaRestoran) {
		super();
		this.kupac = kupac;
		this.stavkeUKorpi = stavkeUKorpi;
		this.zaRestoran = zaRestoran;
	}

	private static final long serialVersionUID = -6665217941415792870L;
	Kupac kupac;
	ArrayList<StavkaUKorpi> stavkeUKorpi = new ArrayList<StavkaUKorpi>();
	String zaRestoran;

	@Override
	public void prikaziJednostavno() {
		for (int i = 0; i < stavkeUKorpi.size(); i++) {
			System.out.print(i + ".");
			stavkeUKorpi.get(i).prikaziJednostavno();
		}
	}

	@Override
	public void prikaziDetaljno() {
		for (int i = 0; i < stavkeUKorpi.size(); i++) {
			System.out.print(i + ".");
			stavkeUKorpi.get(i).prikaziDetaljno();
		}
	}

	public void prikazi() {
		Scanner sken = new Scanner(System.in);
		int opcija;
		System.out.println("Odabrati prikaz: 1: Jednostavan 2: Detaljan");
		try {
			opcija = sken.nextInt();
			sken.nextLine();
		} catch (Exception e) {
			System.out.println("Greska pri unosu.");
			return;
		}
		System.out.println("Korpa za restoran: " + zaRestoran + ", kupca " + kupac.getIme() + " " + kupac.getPrezime());
		if (opcija == 1)
			prikaziJednostavno();
		else if (opcija == 2)
			prikaziDetaljno();
		else {
			System.out.println("Greska pri unosu.");
			return;
		}
	}

	public Kupac getKupac() {
		return kupac;
	}

	public void setKupac(Kupac kupac) {
		this.kupac = kupac;
	}

	public ArrayList<StavkaUKorpi> getStavkeUKorpi() {
		return stavkeUKorpi;
	}

	public void setStavkeUKorpi(ArrayList<StavkaUKorpi> stavkeUKorpi) {
		this.stavkeUKorpi = stavkeUKorpi;
	}
}
