package app;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Scanner;

import apstrakcije.Korisnik;
import apstrakcije.Stavka;
import enumeracije.JedinicaMere;

public class Kupac extends Korisnik implements Serializable {

	public Kupac() {
		this.novcanoStanje = 0;
		this.kucnaAdresa = null;
		this.korpa = new Korpa(this);
		this.porudzbineKupca = null;
		uDatoteku();
	}

	public Kupac(String ime, String prezime, String korisnickoIme, String lozinka) {
		super(ime, prezime, korisnickoIme, lozinka);
		this.novcanoStanje = 10000;
		this.kucnaAdresa = "";
		this.korpa = new Korpa(this);
		uDatoteku();
	}

	public Kupac(int novcanoStanje, String kucnaAdresa, Korpa korpa) {
		super();
		this.novcanoStanje = novcanoStanje;
		this.kucnaAdresa = kucnaAdresa;
		this.korpa = new Korpa(this);
		this.porudzbineKupca = null;
		uDatoteku();
	}

	private static final long serialVersionUID = -6665217941415792871L;
	int novcanoStanje;
	String kucnaAdresa;
	Korpa korpa;
	ArrayList<Porudzbina> porudzbineKupca = new ArrayList<Porudzbina>();

	public void prikaziMeni() {
		Scanner sken = new Scanner(System.in);
		int restoran, opcija;
		System.out.println("Odaberite restoran: ");
		Restoran.prikaziRestorane();
		if (Restoran.restorani.size() == 0)
			return;
		try {
			restoran = sken.nextInt();
		} catch (Exception e) {
			System.out.println("Greska pri unosu.");
			return;
		}
		sken.nextLine();
		System.out.println("Prikazati detaljan ili jednostavan meni? 1: Detaljan, 2: Jednostavan");
		try {
			opcija = sken.nextInt();
		} catch (Exception e) {
			System.out.println("Greska pri unosu.");
			return;
		}
		sken.nextLine();
		if (opcija == 1)
			Restoran.restorani.get(restoran).meni.prikaziDetaljno();
		else if (opcija == 2)
			Restoran.restorani.get(restoran).meni.prikaziJednostavno();
		else
			System.out.println("Neispravno odabrana opcija.");
	}

	public void prikaziMeni(Restoran r) {
		Scanner sken = new Scanner(System.in);
		int opcija;
		System.out.println("Prikazati detaljan ili jednostavan meni? 1: Detaljan, 2: Jednostavan");
		try {
			opcija = sken.nextInt();
		} catch (Exception e) {
			System.out.println("Greska pri unosu.");
			return;
		}
		sken.nextLine();
		if (opcija == 1)
			r.meni.prikaziDetaljno();
		else if (opcija == 2)
			r.meni.prikaziJednostavno();
		else
			System.out.println("Neispravno odabrana opcija.");
	}

	public ArrayList<Restoran> sortirajRestorane() {
		Scanner sken = new Scanner(System.in);
		int opcija = 0;
		ArrayList<Restoran> sortiraniRestorani = new ArrayList<Restoran>();
		sortiraniRestorani = (ArrayList<Restoran>) Restoran.restorani.clone();
		System.out.println("Da li zelite prikaz restorana po: 1: nazivu, 2: minimalnom iznosu za besplatnu dostavu?");
		do {
			try {
				opcija = sken.nextInt();
			} catch (Exception e) {
				System.out.println("Greska pri unosu.");
			}
		} while (opcija != 1 && opcija != 2);
		Restoran rTrenutni = null;
		Restoran rSledeci = null;
		sken.nextLine();
		if (opcija == 1) {
			for (int i = 0; i < sortiraniRestorani.size(); i++) {
				for (int j = 0; j < sortiraniRestorani.size(); j++) {
					try {
						rTrenutni = (Restoran) sortiraniRestorani.get(i).clone();
						rSledeci = (Restoran) sortiraniRestorani.get(j).clone();
					} catch (Exception e) {
					}
					if (((rSledeci.getNaziv()).compareTo(rTrenutni.getNaziv())) > 0) {
						try {
							Restoran priv = (Restoran) rTrenutni.clone();
							sortiraniRestorani.set(i, rSledeci);
							sortiraniRestorani.set(j, priv);
						} catch (Exception e) {
							System.out.println("greska");
						}
					}
				}
			}
		} else if (opcija == 2) {
			for (int i = 0; i < sortiraniRestorani.size(); i++) {
				for (int j = 0; j < sortiraniRestorani.size(); j++) {
					try {
						rTrenutni = (Restoran) sortiraniRestorani.get(i).clone();
						rSledeci = (Restoran) sortiraniRestorani.get(j).clone();
					} catch (Exception e) {
					}
					if (rSledeci.getBesplatnaPreko() > rTrenutni.getBesplatnaPreko()) {
						try {
							Restoran priv = (Restoran) rTrenutni.clone();
							sortiraniRestorani.set(i, rSledeci);
							sortiraniRestorani.set(j, priv);
						} catch (Exception e) {
							System.out.println("greska");
						}
					}
				}
			}
		} else {
			System.out.println("Neispravno uneta opcija.");
			return null;
		}
		return sortiraniRestorani;
	}

	public void dodavanjeStavkeUKorpu() {
		Scanner sken = new Scanner(System.in);
		int odabranSort;
		ArrayList<Restoran> sortirani = this.sortirajRestorane();
		Restoran odabranRestoran = null;
		Restoran.prikaziRestorane(sortirani);
		if (Restoran.restorani.size() == 0) {
			System.out.println("Ni jedan restoran ne radi.");
			return;
		}
		System.out.println("Odaberite neki od ponudjenih restorana.");
		try {
			odabranSort = sken.nextInt();
		} catch (Exception e) {
			System.out.println("Greska pri unosu.");
			return;
		}
		sken.nextLine();
		try {
			odabranRestoran = (Restoran) sortirani.get(odabranSort).clone();
		} catch (CloneNotSupportedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if ((korpa.zaRestoran).compareTo(odabranRestoran.getNaziv()) == 0 || (korpa.zaRestoran).compareTo("") == 0) {
			if ((korpa.zaRestoran).compareTo("") == 0)
				korpa.zaRestoran = odabranRestoran.getNaziv();
			prikaziMeni(odabranRestoran);
			System.out.println("Odaberite stavku za dodavanje u korpu.");
			int stavkaBr;
			try {
				stavkaBr = sken.nextInt();
			} catch (Exception e) {
				System.out.println("Greska pri unosu.");
				return;
			}
			sken.nextLine();
			Stavka stavka = null;
			try {
				stavka = (Stavka) odabranRestoran.meni.stavkeMenija.get(stavkaBr).clone();
			} catch (CloneNotSupportedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			int kolicina = 0, opcija2;
			JedinicaMere jedMere;
			System.out.println("Unesi kolicinu:");
			if (stavka instanceof Pice) {
				try {
					kolicina = sken.nextInt();
				} catch (Exception e) {
					System.out.println("Greska pri unosu.");
					return;
				}
				sken.nextLine();
				System.out.println("Koja je jedinica mere? 1: Litar 2: Mililitar");
				try {
					opcija2 = sken.nextInt();
				} catch (Exception e) {
					System.out.println("Greska pri unosu.");
					return;
				}
				sken.nextLine();
				if (opcija2 == 1)
					jedMere = JedinicaMere.LITAR;
				else if (opcija2 == 2)
					jedMere = JedinicaMere.MILILITAR;
				else {
					System.out.println("Greska u unosu.");
					return;
				}
			} else {
				System.out.println("Unesite kolicinu: ");
				try {
					kolicina = sken.nextInt();
				} catch (Exception e) {
					System.out.println("Greska pri unosu.");
					return;
				}
				sken.nextLine();
				System.out.println("Koja je jedinica mere? 1: Komad 2: Kilogram 3: Gram");
				try {
					opcija2 = sken.nextInt();
				} catch (Exception e) {
					System.out.println("Greska pri unosu.");
					return;
				}
				sken.nextLine();
				if (opcija2 == 1)
					jedMere = JedinicaMere.KOMAD;
				else if (opcija2 == 2)
					jedMere = JedinicaMere.KILOGRAM;
				else if (opcija2 == 3)
					jedMere = JedinicaMere.GRAM;
				else {
					System.out.println("Greska u unosu.");
					return;
				}
			}
			StavkaUKorpi stavkaUKorpi = new StavkaUKorpi(this.korpa, stavka, kolicina, jedMere);
			korpa.stavkeUKorpi.add(stavkaUKorpi);
			uDatotekuIzmene();
		} else {
			System.out.println("Ne mozete mesati stavke iz razlicitih restorana.");
			return;
		}

	}

	public void uklanjanjeStavkeIzKorpe() {
		int opcija;
		if (korpa.stavkeUKorpi.size() == 0) {
			System.out.println("Korpa je prazna, nemate sta obrisati.");
			return;
		}
		Scanner sken = new Scanner(System.in);
		System.out.println("Odaberite opciju: 1: Brisanje pojedinacnih stavki 2: Brisanje svih stavki");
		try {
			opcija = sken.nextInt();
		} catch (Exception e) {
			System.out.println("Greska pri unosu.");
			return;
		}
		sken.nextLine();
		if (opcija == 1) {
			int brisi;
			korpa.prikazi();
			System.out.println("Odaberite stavku za brisanje: ");
			try {
				brisi = sken.nextInt();
			} catch (Exception e) {
				System.out.println("Greska pri unosu.");
				return;
			}
			sken.nextLine();
			korpa.stavkeUKorpi.remove(brisi);
			if (korpa.stavkeUKorpi.size() == 0)
				korpa.zaRestoran = "";
			uDatotekuIzmene();
		} else if (opcija == 2) {
			for (int i = 0; i < korpa.stavkeUKorpi.size(); i++)
				korpa.stavkeUKorpi.remove(i);
			korpa.zaRestoran = "";
			uDatotekuIzmene();
		} else {
			System.out.println("Greska pri unosu.");
			return;
		}
	}

	public int getNovcanoStanje() {
		return novcanoStanje;
	}

	public void setNovcanoStanje(int novcanoStanje) {
		this.novcanoStanje = novcanoStanje;
	}

	public String getKucnaAdresa() {
		return kucnaAdresa;
	}

	public void setKucnaAdresa(String kucnaAdresa) {
		this.kucnaAdresa = kucnaAdresa;
	}

	public Korpa getKorpa() {
		return korpa;
	}

	public void setKorpa(Korpa korpa) {
		this.korpa = korpa;
	}

	public ArrayList<Porudzbina> getPorudzbineKupca() {
		return porudzbineKupca;
	}

	public void setPorudzbineKupca(ArrayList<Porudzbina> porudzbineKupca) {
		this.porudzbineKupca = porudzbineKupca;
	}

}
