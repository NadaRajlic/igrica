package app;

import java.util.Map.Entry;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;

import apstrakcije.Prikaz;
import apstrakcije.Stavka;

public class Meni implements Cloneable, Prikaz, Serializable {

	public Meni() {
		// TODO Auto-generated constructor stub
	}

	private static final long serialVersionUID = -6665217941415792868L;
	public static ArrayList<Meni> meniji = new ArrayList<Meni>();
	ArrayList<Stavka> stavkeMenija = new ArrayList<Stavka>();

	public void dodajStavkuMeniju(Stavka s) {
		stavkeMenija.add(s);
	}

	public void ukloniStavkuIzMenija(Stavka s) {
		stavkeMenija.remove(s);
	}

	@Override
	public void prikaziDetaljno() {
		if (stavkeMenija.size() == 0) {
			System.out.println("Meni je prazan.");
		} else {
			for (int i = 0; i < stavkeMenija.size(); i++) {
				System.out.print(i + ". ");
				stavkeMenija.get(i).prikaziDetaljno();
			}
		}
	}

	@Override
	public void prikaziJednostavno() {
		if (stavkeMenija.size() == 0) {
			System.out.println("Meni je prazan.");
		} else {
			for (int i = 0; i < stavkeMenija.size(); i++) {
				System.out.print(i + ". ");
				stavkeMenija.get(i).prikaziJednostavno();
			}
		}
	}

	public static boolean prikaziMenije() {// true = ima menija, false=nema menija
		if (meniji.size() == 0)
			return false;
		for (int i = 0; i < meniji.size(); i++) {
			System.out.print("\tMENI BROJ " + i + ". ");
			meniji.get(i).prikaziDetaljno();
		}
		return true;
	}

	public static void azurirajMenije() {
		meniji.clear();
		for (Entry<String, Meni> m : VlasnikRestorana.restoranskiMeniji.entrySet()) {
			meniji.add(m.getValue());
			// System.out.println(m.getKey()); m.getValue().prikaziDetaljno();
		}
		uDatoteku();
	}

	public static void uDatoteku() {
		try {
			ObjectOutputStream os = new ObjectOutputStream(new FileOutputStream("datoteke/meniji.csv"));
			os.writeObject(Meni.meniji);
			os.close();
		} catch (IOException e) {
			e.printStackTrace();
			System.out.println(
					"Greska prilikom cuvanja datoteke meniji! Proverite da li datoteka postoji i da li imate pristup.");
		}
	}

	public static void izDatoteke() {
		try {
			ObjectInputStream os = new ObjectInputStream(new FileInputStream("datoteke/meniji.csv"));
			try {
				Meni.meniji = (ArrayList<Meni>) os.readObject();
				os.close();
			} catch (ClassNotFoundException e) {
				// e.printStackTrace();
			}
		} catch (IOException e) {
			if (meniji.size() != 0)
				System.out.println(
						"Greska prilikom ucitavanja datoteke meniji! Proverite da li datoteka postoji i da li imate pristup.");
			// e.printStackTrace();
		}
	}

	@Override
	public Object clone() throws CloneNotSupportedException {
		return super.clone();
	}

	public ArrayList<Stavka> getStavkeMenija() {
		return stavkeMenija;
	}

	public void setStavkeMenija(ArrayList<Stavka> stavkeMenija) {
		this.stavkeMenija = stavkeMenija;
	}

}
