package app;

import java.util.Scanner;

import apstrakcije.Korisnik;

public class Opcije {

	public Opcije() {
		tipKorisnika = 0;
	}

	public Opcije(int tipKorisnik) {
		tipKorisnika = tipKorisnik;
	}

	public static int tipKorisnika = 0;
	static Scanner sken = new Scanner(System.in);
	public static Administrator administrator;
	public static VlasnikRestorana vlasnik;
	public static Kupac kupac;

	public static void prikazi() {
		int opcija = 0;
		switch (tipKorisnika) {
		case 1:
			System.out.println(
					"\n1. Prikaz menija\n2. Dodavanje stavke u korpu\n3. Uklanjanje stavke iz korpe\n4. Porucivanje\n5. Odjava\n");
			try {
				opcija = sken.nextInt();
			} catch (Exception e) {
				System.out.println("Greska pri unosu.");
				return;
			}
			sken.nextLine();
			switch (opcija) {
			case 1:
				kupac.prikaziMeni();
				prikazi();
				break;
			case 2:
				kupac.dodavanjeStavkeUKorpu();
				prikazi();
				break;

			case 3:
				kupac.uklanjanjeStavkeIzKorpe();
				prikazi();
				break;
			case 4:
				System.out.println("Dostava trenutno nije dostupna.");
				break;
			case 5:
				Korisnik.odjava();
				break;
			default:
				System.out.println("Neispravno uneta opcija.");
			}
			break;
		case 2:
			System.out.println(
					"\n1. Kreiranje menija\n2. Dodavanje stavke u meni\n3. Brisanje stavke iz menija\n4. Prikaz menija\n5. Izmena cene stavke iz menija\n6. Odjava\n");
			try {
				opcija = sken.nextInt();
			} catch (Exception e) {
				System.out.println("Greska pri unosu.");
				return;
			}
			sken.nextLine();
			switch (opcija) {
			case 1:
				vlasnik.kreiranjeMenija();
				prikazi();
				break;
			case 2:
				vlasnik.dodavanjeStavkeUMeni();
				prikazi();
				break;
			case 3:
				vlasnik.brisanjeStavkeIzMenija();
				prikazi();
				break;
			case 4:
				vlasnik.prikaziMeni();
				prikazi();
				break;
			case 5:
				vlasnik.izmenaCeneStavkeIzMenija();
				prikazi();
				break;
			case 6:
				Korisnik.odjava();
				break;
			}
			break;
		case 3:
			System.out.println(
					"\n1. Dodavanje restorana\n2. Kreiranje korisnika\n3. Dodavanje vlasnika restorana\n4. Odjava\n");
			try {
				opcija = sken.nextInt();
			} catch (Exception e) {
				System.out.println("Greska pri unosu.");
			}
			sken.nextLine();
			switch (opcija) {
			case 1:
				try {
					administrator.dodajRestoran();
				} catch (Exception e) {
					System.out.println("Ime restorana vec postoji.");
				}
				prikazi();
				break;
			case 2:
				administrator.kreirajKorisnika();
				prikazi();
				break;
			case 3:
				administrator.dodajVlasnika();
				prikazi();
				break;
			case 4:
				Korisnik.odjava();
				break;
			default:
				System.out.println("Trazena opcija ne postoji.");

			}
			break;

		default:

		}
	}

	public static void unesi() {
		String uKorisnicko = "";
		String uLozinka = "";
		int tK = 0;
		Scanner sken = new Scanner(System.in);
		do {
			System.out.println("Unesite korisnicko ime: ");
			try {
				uKorisnicko = sken.nextLine();
			} catch (Exception e) {
				System.out.println("Greska pri unosu.");
				return;
			}
			System.out.println("Unesite lozinku: ");
			try {
				uLozinka = sken.nextLine();
			} catch (Exception e) {
				System.out.println("Greska pri unosu.");
				return;
			}
			if (Korisnik.prijava(uKorisnicko, uLozinka) == 0)
				System.out.println("Pogresna kombinacija korisnickog imena i lozinke.");
		} while ((tK = Korisnik.prijava(uKorisnicko, uLozinka)) == 0);
		tipKorisnika = tK;
		prikazi();
	}

	public static void ponisti() {
		administrator = null;
		vlasnik = null;
		kupac = null;
	}

	public int getTipKorisnika() {
		return tipKorisnika;
	}
}
