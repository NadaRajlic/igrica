package app;

import java.io.Serializable;
import java.util.ArrayList;

import apstrakcije.Stavka;
import enumeracije.JedinicaMere;
import enumeracije.TipPica;

public class Pice extends Stavka implements Serializable {

	public Pice() {
		// TODO Auto-generated constructor stub
	}

	public Pice(String naziv, int kolicina, JedinicaMere jedinicaMere, int cena, TipPica tipPica) {
		super();
		this.naziv = naziv;
		this.kolicina = kolicina;
		this.jedinicaMere = jedinicaMere;
		this.cena = cena;
		this.tipoviPica.add(tipPica);
	}

	ArrayList<TipPica> tipoviPica = new ArrayList<TipPica>();
	private static final long serialVersionUID = -6665217941415792872L;

	public ArrayList<TipPica> getTipoviPica() {
		return tipoviPica;
	}

	public boolean dodajTipPica(TipPica t) {
		boolean jedinstven = true;
		String prosledjeno = t.toString().toLowerCase();
		String trenutno;
		for (int i = 0; i < tipoviPica.size(); i++) {
			trenutno = tipoviPica.get(i).toString().toLowerCase();
			if (trenutno.contains(prosledjeno)) //== true)
			{
				jedinstven = false;
			}
			if (prosledjeno.contains(trenutno)) //== true) 
			{
				jedinstven = false;
			}

		}
		if (jedinstven) {
			return true;
		} else {
			return false;
		}
	}

	@Override
	public void prikaziDetaljno() {
		System.out.print("Naziv: " + naziv + ", kolicina: " + kolicina + ", jedinica mere: " + jedinicaMere.toString()
				+ ", cena: " + cena + ", Tip: ");
		for (TipPica i : tipoviPica)
			System.out.print(i.toString() + ", ");
		System.out.println();
	}

}
