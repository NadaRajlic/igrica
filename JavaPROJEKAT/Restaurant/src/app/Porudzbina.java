package app;

import java.io.Serializable;
import java.util.ArrayList;
import enumeracije.TipDostave;

public class Porudzbina implements Serializable {

	public Porudzbina() {
		super();
		this.kupac = null;
		this.adresaIsporuke = null;
		this.tipDostave = null;
		this.poruceneStavke = null;
	}

	public Porudzbina(Kupac kupac, String adresaIsporuke, TipDostave tipDostave,
			ArrayList<StavkaUKorpi> poruceneStavke) {
		super();
		this.kupac = kupac;
		this.adresaIsporuke = adresaIsporuke;
		this.tipDostave = tipDostave;
		this.poruceneStavke = poruceneStavke;
	}

	private static final long serialVersionUID = -6665217941415792867L;
	public static ArrayList<Porudzbina> porudzbine = new ArrayList<Porudzbina>();
	Kupac kupac;
	String adresaIsporuke;
	TipDostave tipDostave;
	ArrayList<StavkaUKorpi> poruceneStavke = new ArrayList<StavkaUKorpi>();

}
