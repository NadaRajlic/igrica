package app;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;

public class Restoran implements Cloneable, Serializable {

	public Restoran() {
		this.besplatnaPreko = 0;
		this.naziv = "";
	}

	public Restoran(String naziv, int besplatnaPreko) {
		super();
		boolean jedinstven = true;
		for (int i = 0; i < restorani.size(); i++) {
			if (naziv.compareTo(restorani.get(i).naziv) == 0)
				jedinstven = false;
		}
		if (!jedinstven) {
			throw new IllegalArgumentException("Naziv restorana mora biti jedinstven.");
		} else {
			this.naziv = naziv;
			this.besplatnaPreko = besplatnaPreko;
			uDatoteku();
		}
	}

	private static final long serialVersionUID = -6665217941415792866L;
	public static ArrayList<Restoran> restorani = new ArrayList<Restoran>();
	protected String naziv;
	protected int besplatnaPreko;
	protected Meni meni = new Meni();
	protected VlasnikRestorana vlasnik;

	@Override
	public Object clone() throws CloneNotSupportedException {
		return super.clone();
	}

	public void prikazi() {
			System.out.println("Naziv: " + naziv + ", dostava besplatna preko: " + besplatnaPreko + ", vlasnik: " + vlasnik.getIme());
	}

	public static void prikaziRestorane() {
		if (restorani.size() == 0) {
			System.out.println("Nema restorana.");
			return;
		}
		for (int i = 0; i < restorani.size(); i++) {
			System.out.print(i + ". ");
			restorani.get(i).prikazi();
		}
	}

	public static void prikaziRestorane(ArrayList<Restoran> pRestorani) {
		if (pRestorani.size() == 0) {
			System.out.println("Nema restorana.");
			return;
		}
		for (int i = 0; i < pRestorani.size(); i++) {
			System.out.print(i + ". ");
			pRestorani.get(i).prikazi();
		}
	}

	public void uDatoteku() {
		try {
			ObjectOutputStream os = new ObjectOutputStream(new FileOutputStream("datoteke/restorani.csv"));
			Restoran.restorani.add(this);
			os.writeObject(Restoran.restorani);
			os.close();
		} catch (IOException e) {
			e.printStackTrace();
			System.out.println(
					"Greska prilikom cuvanja datoteke restorani! Proverite da li datoteka postoji i da li imate pristup.");
		}
	}

	public static void uDatotekuIzmene() {
		try {
			ObjectOutputStream os = new ObjectOutputStream(new FileOutputStream("datoteke/restorani.csv"));
			os.writeObject(Restoran.restorani);
			os.close();
		} catch (IOException e) {
			e.printStackTrace();
			System.out.println(
					"Greska prilikom cuvanja datoteke restorani! Proverite da li datoteka postoji i da li imate pristup.");
		}
	}

	public static void izDatoteke() {
		try {
			ObjectInputStream os = new ObjectInputStream(new FileInputStream("datoteke/restorani.csv"));
			try {
				Restoran.restorani = (ArrayList<Restoran>) os.readObject();
				os.close();
			} catch (ClassNotFoundException e) {
				// e.printStackTrace();
			}
		} catch (IOException e) {
			if (restorani.size() != 0)
				System.out.println(
						"Greska prilikom ucitavanja datoteke restorani! Proverite da li datoteka postoji i da li imate pristup.");
			// e.printStackTrace();
		}
	}

	public String getNaziv() {
		return naziv;
	}

	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}

	public int getBesplatnaPreko() {
		return besplatnaPreko;
	}

	public void setBesplatnaPreko(int besplatnaPreko) {
		this.besplatnaPreko = besplatnaPreko;
	}

	public Meni getMeni() {
		return meni;
	}

	public void setMeni(Meni meni) {
		this.meni = meni;
	}

	public VlasnikRestorana getVlasnik() {
		return vlasnik;
	}

	public void setVlasnik(VlasnikRestorana vlasnik) {
		this.vlasnik = vlasnik;
	}
}
