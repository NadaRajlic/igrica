package app;

import java.io.Serializable;
import apstrakcije.Stavka;
import enumeracije.JedinicaMere;

public class StavkaUKorpi implements Serializable {

	public StavkaUKorpi() {
		super();
		this.korpa = null;
		this.stavka = null;
		this.kolicina = 0;
		this.jedinicaMere = null;
		this.cena = 0;
	}

	public StavkaUKorpi(Korpa korpa, Stavka stavka, int kolicina, JedinicaMere jedinicaMere) {
		super();
		this.korpa = korpa;
		this.stavka = stavka;
		this.kolicina = kolicina;
		this.jedinicaMere = jedinicaMere;
		this.cena = stavka.getCena();
	}

	Korpa korpa;
	Stavka stavka;
	int kolicina;
	JedinicaMere jedinicaMere;
	int cena;
	private static final long serialVersionUID = -6665217941415792865L;

	public void prikaziJednostavno() {
		System.out.println(stavka.getNaziv() + " " + ", cena: " + cena);
	}

	public void prikaziDetaljno() {
		System.out.println("Naziv: " + stavka.getNaziv() + ", kolicina: " + kolicina + ", jedinica mere: "
				+ jedinicaMere.toString() + ", cena: " + cena);
	}

	public Korpa getKorpa() {
		return korpa;
	}

	public void setKorpa(Korpa korpa) {
		this.korpa = korpa;
	}

	public Stavka getStavka() {
		return stavka;
	}

	public void setStavka(Stavka stavka) {
		this.stavka = stavka;
	}

	public int getKolicina() {
		return kolicina;
	}

	public void setKolicina(int kolicina) {
		this.kolicina = kolicina;
	}

	public JedinicaMere getJedinicaMere() {
		return jedinicaMere;
	}

	public void setJedinicaMere(JedinicaMere jedinicaMere) {
		this.jedinicaMere = jedinicaMere;
	}

	public int getCena() {
		return cena;
	}

	public void setCena(int cena) {
		this.cena = cena;
	}
}
