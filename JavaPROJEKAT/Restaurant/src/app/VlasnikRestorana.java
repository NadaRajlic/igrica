package app;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Scanner;

import apstrakcije.Korisnik;
import apstrakcije.Stavka;
import enumeracije.JedinicaMere;
import enumeracije.TipJela;
import enumeracije.TipPica;

public class VlasnikRestorana extends Korisnik implements Serializable {

	public VlasnikRestorana() {
		// TODO Auto-generated constructor stub
	}

	public VlasnikRestorana(String ime, String prezime, String korisnickoIme, String lozinka) {
		super(ime, prezime, korisnickoIme, lozinka);
		uDatoteku();
	}

	private static final long serialVersionUID = -6665217941415792864L;
	public static HashMap<String, Meni> restoranskiMeniji = new HashMap<String, Meni>();
	ArrayList<Restoran> restoraniVlasnika = new ArrayList<Restoran>();

	public void prikaziMojeRestorane() {
		for (int i = 0; i < restoraniVlasnika.size(); i++) {
			System.out.print(i + ". ");
			restoraniVlasnika.get(i).prikazi();
		}
	}

	public void kreiranjeMenija() {
		Scanner sken = new Scanner(System.in);
		int opcija, opcija2, uRestoran;
		if (restoraniVlasnika.size() == 0) {
			System.out.println("Nemate ni jedan restoran!");
			return;
		} else {
			System.out.println("Izaberite restoran za koji zelite da kreirate meni: ");
			prikaziMojeRestorane();
			try {
				uRestoran = sken.nextInt();
			} catch (Exception e) {
				System.out.println("Greska pri unosu.");
				return;
			}
			sken.nextLine();
			if(uRestoran>=restoraniVlasnika.size() || uRestoran<0) {
				System.out.println("Taj restoran ne postoji.");
				return;
			}
			if (restoraniVlasnika.get(uRestoran).getMeni().stavkeMenija.size() > 0) {
				System.out.println("Ovaj restoran vec ima meni.");
				return;
			}
			System.out.println("Da li zelite da dodate postojeci ili kreirate novi meni? 1: Postojeci 2: Novi");
			try {
				opcija = sken.nextInt();
			} catch (Exception e) {
				System.out.println("Greska pri unosu.");
				return;
			}
			sken.nextLine();
			if (opcija == 1) {
				if (!Meni.prikaziMenije())
					System.out.println("Nema dostupnih menija.");
				else {
					System.out.println("Izaberite jedan od menija.");
					try {
						opcija2 = sken.nextInt();
					} catch (Exception e) {
						System.out.println("Greska pri unosu.");
						return;
					}
					sken.nextLine();
					if (opcija2 > Meni.meniji.size() || opcija2 < 0) {
						System.out.println("Greska u unosu.");
						return;
					}
					restoraniVlasnika.get(uRestoran).setMeni(Meni.meniji.get(opcija2));
					System.out.println("Meni uspesno dodat.");
					restoranskiMeniji.put(restoraniVlasnika.get(uRestoran).getNaziv(),
							restoraniVlasnika.get(uRestoran).getMeni());
					uDatotekuIzmene();
					Restoran.uDatotekuIzmene();
					uDatotekuMap();
					Meni.azurirajMenije();
				}
			} else if (opcija == 2) {
				restoraniVlasnika.get(uRestoran).setMeni(new Meni());
				System.out.println("Novi meni uspesno kreiran. Mozete dodati stavke u njega.");
			} else {
				System.out.println("Neispravno odabrana opcija.");
			}
		}
	}

	public void dodavanjeStavkeUMeni() {
		Scanner sken = new Scanner(System.in);
		int opcija, opcija2, uRestoran;
		if (restoraniVlasnika.size() == 0) {
			System.out.println("Nemate ni jedan restoran!");
			return;
		} else {
			System.out.println("Izaberite restoran za ciji meni zelite da odaberete stavku: ");
			prikaziMojeRestorane();
			try {
				uRestoran = sken.nextInt();
			} catch (Exception e) {
				System.out.println("Greska pri unosu.");
				return;
			}
			sken.nextLine();
			System.out.println("Da li se dodaje pice ili jelo? 1: Pice, 2: Jelo");
			try {
				opcija = sken.nextInt();
			} catch (Exception e) {
				System.out.println("Greska pri unosu.");
				return;
			}
			sken.nextLine();
			if (opcija == 1) {
				JedinicaMere jedMere;
				TipPica tipPica;
				String naziv;
				int kolicina, cena;
				try {
					System.out.println("Unesite naziv pica: ");
					naziv = sken.nextLine();
					System.out.println("Unesite kolicinu: ");
					kolicina = sken.nextInt();
					sken.nextLine();
					System.out.println("Koja je jedinica mere? 1: Litar 2: Mililitar");
					opcija2 = sken.nextInt();
					sken.nextLine();
					if (opcija2 == 1)
						jedMere = JedinicaMere.LITAR;
					else if (opcija2 == 2)
						jedMere = JedinicaMere.MILILITAR;
					else {
						System.out.println("Greska u unosu.");
						return;
					}
					System.out.println("Odaberite cenu: ");
					cena = sken.nextInt();
					sken.nextLine();
					System.out.println(
							"Odaberite tip pica:\n1. Gazirano, 2: Negazirano, 3. Alkoholno, 4. Bezalkoholno, 5. Hladno, 6. Nehladno");
					opcija2 = sken.nextInt();
				} catch (Exception e) {
					System.out.println("Greska pri unosu podataka.");
					return;
				}
				sken.nextLine();
				switch (opcija2) {
				case 1:
					tipPica = TipPica.GAZIRANO;
					break;
				case 2:
					tipPica = TipPica.NEGAZIRANO;
					break;
				case 3:
					tipPica = TipPica.ALKOHOLNO;
					break;
				case 4:
					tipPica = TipPica.BEZALKOHOLNO;
					break;
				case 5:
					tipPica = TipPica.HLADNO;
					break;
				case 6:
					tipPica = TipPica.NEHLADNO;
					break;
				default:
					System.out.println("Greska pri izboru.");
					return;
				}
				Pice p = new Pice(naziv, kolicina, jedMere, cena, tipPica);
				do {
					System.out.println("Zelite li da dodate jos tipova picu? 1: Ne 2: Da");
					opcija = sken.nextInt();
					sken.nextLine();
					if (opcija == 2) {
						System.out.println(
								"Odaberite tip pica:\n1. Gazirano, 2: Negazirano, 3. Alkoholno, 4. Bezalkoholno, 5. Hladno, 6. Nehladno");
						try {
							opcija2 = sken.nextInt();
							sken.nextLine();
							switch (opcija2) {
							case 1:
								tipPica = TipPica.GAZIRANO;
								break;
							case 2:
								tipPica = TipPica.NEGAZIRANO;
								break;
							case 3:
								tipPica = TipPica.ALKOHOLNO;
								break;
							case 4:
								tipPica = TipPica.BEZALKOHOLNO;
								break;
							case 5:
								tipPica = TipPica.HLADNO;
								break;
							case 6:
								tipPica = TipPica.NEHLADNO;
								break;
							default:
								System.out.println("Greska pri izboru.");
								return;
							}
						} catch (Exception e) {
							System.out.println("Greska pri unosu.");
							return;
						}
						if (!p.dodajTipPica(tipPica))
							System.out.println("Ne mozete dodati tu vrstu pica!");
						else
							p.tipoviPica.add(tipPica);
					}
				} while (opcija != 1);
				Restoran privRestoran = restoraniVlasnika.get(uRestoran);
				privRestoran.meni.stavkeMenija.add(p);
				restoraniVlasnika.set(uRestoran, privRestoran);
				restoranskiMeniji.put(restoraniVlasnika.get(uRestoran).getNaziv(),
						restoraniVlasnika.get(uRestoran).getMeni());
				uDatotekuIzmene();
				Restoran.uDatotekuIzmene();
				uDatotekuMap();
				Meni.azurirajMenije();
				// sacuvati
			}

			else if (opcija == 2) {
				JedinicaMere jedMere;
				TipJela tipJela;
				String naziv;
				int kolicina, cena;
				System.out.println("Unesite naziv jela: ");
				try {
					naziv = sken.nextLine();
					System.out.println("Unesite kolicinu: ");
					kolicina = sken.nextInt();
					sken.nextLine();
					System.out.println("Koja je jedinica mere? 1: Komad 2: Kilogram 3: Gram");
					opcija2 = sken.nextInt();
					sken.nextLine();
					if (opcija2 == 1)
						jedMere = JedinicaMere.KOMAD;
					else if (opcija2 == 2)
						jedMere = JedinicaMere.KILOGRAM;
					else if (opcija2 == 3)
						jedMere = JedinicaMere.GRAM;
					else {
						System.out.println("Greska u unosu.");
						return;
					}
					System.out.println("Odaberite cenu: ");
					cena = sken.nextInt();
					sken.nextLine();
					System.out.println("Odaberite tip jela:\n1. Glavno jelo, 2: Predjelo, 3. Salata, 4. Desert");
					opcija2 = sken.nextInt();
					sken.nextLine();
					switch (opcija2) {
					case 1:
						tipJela = TipJela.GLAVNO_JELO;
						break;
					case 2:
						tipJela = TipJela.PREDJELO;
						break;
					case 3:
						tipJela = TipJela.SALATA;
						break;
					case 4:
						tipJela = TipJela.DESERT;
						break;
					default:
						System.out.println("Greska pri izboru.");
						return;
					}
				} catch (Exception e) {
					System.out.println("Greska pri unosu.");
					return;
				}
				Jelo j = new Jelo(naziv, kolicina, jedMere, cena, tipJela);
				Restoran privRestoran = restoraniVlasnika.get(uRestoran);
				privRestoran.meni.stavkeMenija.add(j);
				restoraniVlasnika.set(uRestoran, privRestoran);
				restoranskiMeniji.put(restoraniVlasnika.get(uRestoran).getNaziv(),
						restoraniVlasnika.get(uRestoran).getMeni());
				uDatoteku();
				Restoran.uDatotekuIzmene();
				uDatotekuMap();
				Meni.azurirajMenije();
			} else {
				System.out.println("Neispravno odabrana opcija.");
			}
		}
	}

	public void prikaziMeni() {
		Scanner sken = new Scanner(System.in);
		int restoran, opcija;
		System.out.println("Odaberite restoran: ");
		prikaziMojeRestorane();
		try {
			restoran = sken.nextInt();
		} catch (Exception e) {
			System.out.println("Greska pri unosu.");
			return;
		}
		sken.nextLine();
		System.out.println("Prikazati detaljan ili jednostavan meni? 1: Detaljan, 2: Jednostavan");
		try {
			opcija = sken.nextInt();
		} catch (Exception e) {
			System.out.println("Greska pri unosu.");
			return;
		}
		sken.nextLine();
		if (opcija == 1)
			restoraniVlasnika.get(restoran).meni.prikaziDetaljno();
		else if (opcija == 2)
			restoraniVlasnika.get(restoran).meni.prikaziJednostavno();
		else
			System.out.println("Neispravno odabrana opcija.");
	}

	public boolean prikaziMeni(int r) {
		Scanner sken = new Scanner(System.in);
		int opcija = 0;
		System.out.println("Prikazati detaljan ili jednostavan meni? 1: Detaljan, 2: Jednostavan");
		do {
			try {
				opcija = sken.nextInt();
			} catch (Exception e) {
				System.out.println("Greska pri unosu.");
			}
		} while (opcija != 1 && opcija != 2);

		sken.nextLine();
		if (opcija == 1) {
			restoraniVlasnika.get(r).meni.prikaziDetaljno();
			return true;
		} else if (opcija == 2) {
			restoraniVlasnika.get(r).meni.prikaziJednostavno();
			return true;
		} else {
			System.out.println("Neispravno odabrana opcija.");
			return false;
		}
	}

	public void brisanjeStavkeIzMenija() {
		Scanner sken = new Scanner(System.in);
		int restoran, brisi;
		System.out.println("Odaberite restoran: ");
		prikaziMojeRestorane();
		try {
			restoran = sken.nextInt();
		} catch (Exception e) {
			System.out.println("Greska pri unosu.");
			return;
		}
		sken.nextLine();
		if (prikaziMeni(restoran) == false)
			return;
		if (restoraniVlasnika.get(restoran).getMeni().stavkeMenija.size() == 0)
			return;
		System.out.println("Odaberite neku od gore navedenih stavki za brisanje.");
		try {
			brisi = sken.nextInt();
		} catch (Exception e) {
			System.out.println("Greska pri unosu.");
			return;
		}
		sken.nextLine();
		restoraniVlasnika.get(restoran).meni.stavkeMenija.remove(brisi);
		restoranskiMeniji.put(restoraniVlasnika.get(restoran).getNaziv(), restoraniVlasnika.get(restoran).getMeni());
		Restoran.uDatotekuIzmene();
		uDatotekuMap();
		Meni.azurirajMenije();

		// sacuvaj
	}

	public void izmenaCeneStavkeIzMenija() {
		Scanner sken = new Scanner(System.in);
		System.out.println("Odaberite restoran: ");
		prikaziMojeRestorane();
		int restoran, izmena, novacena;
		try {
			restoran = sken.nextInt();
		} catch (Exception e) {
			System.out.println("Greska pri unosu.");
			return;
		}
		sken.nextLine();
		if (prikaziMeni(restoran) == false)
			return;
		if (restoraniVlasnika.get(restoran).getMeni().stavkeMenija.size() == 0)
			return;
		System.out.println("Odaberite neku od gore navedenih stavki za izmenu.");
		try {
			izmena = sken.nextInt();
		} catch (Exception e) {
			System.out.println("Greska pri unosu.");
			return;
		}
		sken.nextLine();
		System.out.println("Unesite novu cenu stavke: ");
		try {
			novacena = sken.nextInt();
		} catch (Exception e) {
			System.out.println("Greska pri unosu.");
			return;
		}
		sken.nextLine();
		Stavka s = restoraniVlasnika.get(restoran).meni.stavkeMenija.get(izmena);
		s.setCena(novacena);
		restoraniVlasnika.get(restoran).meni.stavkeMenija.set(izmena, s);
		restoranskiMeniji.put(restoraniVlasnika.get(restoran).getNaziv(), restoraniVlasnika.get(restoran).getMeni());
		Restoran.uDatotekuIzmene();
		uDatotekuMap();
		Meni.azurirajMenije();

		// sacuvaj
	}

	public void uDatotekuMap() {
		try {
			ObjectOutputStream os = new ObjectOutputStream(new FileOutputStream("datoteke/restoranMeni.csv"));
			os.writeObject(VlasnikRestorana.restoranskiMeniji);
			os.close();
		} catch (IOException e) {
			e.printStackTrace();
			System.out.println(
					"Greska prilikom cuvanja datoteke restoranMeni! Proverite da li datoteka postoji i da li imate pristup.");
		}
	}

	public static void izDatotekeMap() {
		try {
			ObjectInputStream os = new ObjectInputStream(new FileInputStream("datoteke/restoranMeni.csv"));
			try {
				VlasnikRestorana.restoranskiMeniji = (HashMap<String, Meni>) os.readObject();
				os.close();
			} catch (ClassNotFoundException e) {
				// e.printStackTrace();
			}
		} catch (IOException e) {
			if (restoranskiMeniji.size() != 0)
				System.out.println(
						"Greska prilikom ucitavanja datoteke restoranMeni! Proverite da li datoteka postoji i da li imate pristup.");
			// e.printStackTrace();
		}
	}

}
