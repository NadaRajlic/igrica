package apstrakcije;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Scanner;
import app.Opcije;

import app.Administrator;
import app.VlasnikRestorana;
import app.Kupac;

public abstract class Korisnik implements Cloneable, Serializable {
	public Korisnik() {
		super();
		this.ime = "";
		this.prezime = "";
		this.korisnickoIme = "";
		this.lozinka = "";
	}

	public Korisnik(String ime, String prezime, String korisnickoIme, String lozinka) {
		boolean jedinstven = true;
		for (int i = 0; i < korisnici.size(); i++) {
			if (korisnickoIme.compareTo(korisnici.get(i).korisnickoIme) == 0)
				jedinstven = false;
		}
		if (!jedinstven) {
			System.out.println("Korisnicko ime mora biti jedinstveno.");
		} else {
			this.ime = ime;
			this.prezime = prezime;
			this.korisnickoIme = korisnickoIme;
			this.lozinka = lozinka;
			System.out.println("Novi korisnik je uspesno kreiran.");
		}
	}

	public static ArrayList<Korisnik> korisnici = new ArrayList<Korisnik>();
	private static final long serialVersionUID = -6665217941415792861L;

	protected String ime;
	protected String prezime;
	protected String korisnickoIme;
	protected String lozinka;

	// povratne vrednosti: 3=administrator, 2= Vlasnik, 1=Kupac 0=ne postoji
	public static int prijava(String korisnickoIme, String lozinka) {

		int i = 0;
		boolean postoji = false;
		while (i < korisnici.size() && !postoji) {
			if (korisnickoIme.compareTo(korisnici.get(i).korisnickoIme) == 0
					&& lozinka.compareTo(korisnici.get(i).lozinka) == 0) {
				postoji = true;
			} else
				i++;
		}
		if (postoji) {
			if (korisnici.get(i) instanceof Administrator) {
				Opcije.administrator = (Administrator) korisnici.get(i);
				return 3;
			} else if (korisnici.get(i) instanceof VlasnikRestorana) {
				Opcije.vlasnik = (VlasnikRestorana) korisnici.get(i);
				return 2;
			} else {
				Opcije.kupac = (Kupac) korisnici.get(i);
				return 1;
			}
		} else
			return 0;

	}

	public static void odjava() {
		System.out.println("Zelite li da napustite aplikaciju? 1: Da, 2: Ne");
		int opcija;
		Scanner sken = new Scanner(System.in);
		try {
			opcija = sken.nextInt();
		} catch (Exception e) {
			System.out.println("Greska pri unosu.");
			return;
		}
		if (opcija == 2) {
			sken.nextLine();
			Opcije.tipKorisnika = 0;
			System.out.println("Uspesna odjava.");
			Opcije.ponisti();
			Opcije.unesi();
		} else
			System.exit(0);
	}
	@Override
	public Object clone() throws CloneNotSupportedException {
		return super.clone();
	}

	public void uDatoteku() {
		try {
			ObjectOutputStream os = new ObjectOutputStream(new FileOutputStream("datoteke/korisnici.csv"));
			Korisnik.korisnici.add(this);
			os.writeObject(Korisnik.korisnici);
			os.close();
		} catch (IOException e) {
			e.printStackTrace();
			System.out.println(
					"Greska prilikom cuvanja datoteke korisnici! Proverite da li datoteka postoji i da li imate pristup.");
		}
	}

	public void uDatotekuIzmene() {
		try {
			ObjectOutputStream os = new ObjectOutputStream(new FileOutputStream("datoteke/korisnici.csv"));
			os.writeObject(Korisnik.korisnici);
			os.close();
		} catch (IOException e) {
			e.printStackTrace();
			System.out.println(
					"Greska prilikom cuvanja datoteke korisnici! Proverite da li datoteka postoji i da li imate pristup.");
		}
	}

	public static void izDatoteke() {
		try {
			ObjectInputStream os = new ObjectInputStream(new FileInputStream("datoteke/korisnici.csv"));
			try {
				Korisnik.korisnici = (ArrayList<Korisnik>) os.readObject();
				os.close();
			} catch (ClassNotFoundException e) {
			// e.printStackTrace();
			}
		} catch (IOException e) {
			if (korisnici.size() != 0)
				System.out.println(
						"Greska prilikom ucitavanja datoteke korisnici! Proverite da li datoteka postoji i da li imate pristup.");
			// e.printStackTrace();
		}
	}

	public static ArrayList<Korisnik> getKorisnici() {
		return korisnici;
	}

	public static void setKorisnici(ArrayList<Korisnik> korisnici) {
		Korisnik.korisnici = korisnici;
	}

	public String getIme() {
		return ime;
	}

	public void setIme(String ime) {
		this.ime = ime;
	}

	public String getPrezime() {
		return prezime;
	}

	public void setPrezime(String prezime) {
		this.prezime = prezime;
	}

	public String getKorisnickoIme() {
		return korisnickoIme;
	}

	public void setKorisnickoIme(String korisnickoIme) {
		this.korisnickoIme = korisnickoIme;
	}

	public String getLozinka() {
		return lozinka;
	}

	public void setLozinka(String lozinka) {
		this.lozinka = lozinka;
	}


}
