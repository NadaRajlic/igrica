package apstrakcije;

public interface Prikaz {
	public void prikaziDetaljno();

	public void prikaziJednostavno();
}
