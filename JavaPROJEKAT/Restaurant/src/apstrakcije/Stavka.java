package apstrakcije;

import java.io.Serializable;

import enumeracije.JedinicaMere;

public abstract class Stavka implements Cloneable, Prikaz, Serializable {

	public Stavka() {
		this.naziv = "";
		this.kolicina = 0;
		this.jedinicaMere = null;
		this.cena = 0;
	}

	private static final long serialVersionUID = -6665217941415792863L;
	protected String naziv;
	protected int kolicina;
	protected JedinicaMere jedinicaMere;
	protected int cena;

	@Override
	public Object clone() throws CloneNotSupportedException {
		return super.clone();
	}

	public String getNaziv() {
		return naziv;
	}

	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}

	public int getKolicina() {
		return kolicina;
	}

	public void setKolicina(int kolicina) {
		this.kolicina = kolicina;
	}

	public JedinicaMere getJedinicaMere() {
		return jedinicaMere;
	}

	public void setJedinicaMere(JedinicaMere jedinicaMere) {
		this.jedinicaMere = jedinicaMere;
	}

	public int getCena() {
		return cena;
	}

	public void setCena(int cena) {
		this.cena = cena;
	}

	@Override
	public abstract void prikaziDetaljno();


	@Override
	public void prikaziJednostavno() {
		System.out.println("Naziv: " + naziv + ", cena: " + cena);
	}

}
