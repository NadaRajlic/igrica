package enumeracije;

public enum TipPica {
	GAZIRANO, 
	NEGAZIRANO, 
	ALKOHOLNO, 	
	BEZALKOHOLNO, 
	HLADNO, 
	NEHLADNO
}